export interface Recordatorio {
    idRecordatorio:number;
    titulo: string;
    descripcion: string;
    usuario:string;
    fecha:Date;
  }