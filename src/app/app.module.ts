import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PrincipalComponent } from './principal/principal.component';
import { NotaComponent } from './nota/nota.component';
import { SignupComponent } from './signup/signup.component';
import { RecordatorioComponent } from './recordatorio/recordatorio.component';
import { HttpClientModule }    from '@angular/common/http';
import { RestService } from './rest.service';
NotaComponent

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PrincipalComponent,
    NotaComponent,
    SignupComponent,
    RecordatorioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [RestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
