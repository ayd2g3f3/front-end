import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  alerta = '';
  mensaje = '';
  password = "";
  nombre = "";
  lastName = "";
  balance: number = 0;
  correo = "";


  constructor(private rest: RestService, private router: Router) { 
  }

  ngOnInit() {
  }

  createSignupObject( password: string, nombre: string,lastName: string, correo: string): any {
      return { 
        correo: correo,
        nombre: nombre +lastName,  
        password: password 
      };
  }

  async signup() {
    if (this.checkFields()) {
      let obj = this.createSignupObject(this.password,this.nombre,this.lastName,this.correo);
      let response = await this.rest.PostRequest('/agregarUsuario', obj).toPromise();
      console.log(response);

      if (response.estado) {
        console.log("todo bien")
        this.mensaje = 'Su cuenta ha sido creada';
        
        setTimeout(() => this.router.navigate(['']), 2000);
      }
      else {
        this.alerta = response.mensaje;
        setTimeout(() => this.alerta = '', 2000);
      }
    }
  }

  checkFields() {
    
    if (this.password === '') {
      this.alerta = 'Debe ingresar su contrasenia';
      setTimeout(() => this.alerta = '', 2000);
      return false;
    }
    else if (this.nombre === '') {
      this.alerta = 'Debe ingresar su nombre';
      setTimeout(() => this.alerta = '', 2000);
      return false;
    }
    else if (this.lastName === '') {
      this.alerta = 'Debe ingresar sus apellidos';
      setTimeout(() => this.alerta = '', 2000);
      return false;
    }
    else if (this.correo === '') {
      this.alerta = 'Debe ingresar su correo';
      setTimeout(() => this.alerta = '', 2000);
      return false;
    }

    return true;
  }
}
