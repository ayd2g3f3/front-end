import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  correo = '';
  password = '';
  alerta = '';

  constructor(private rest: RestService, private router: Router) { }

  ngOnInit() {
  }

  createLoginObject(account: string, password: string): any {
    return { account: account, password: password };
  }

  async login() {
    if (this.checkFields()) {
      let obj = this.createLoginObject(this.correo, this.password);
      let acceso = await this.rest.PostRequest('login', obj).toPromise();
      if (acceso.estado) {
        acceso.mensaje.balance = parseInt(acceso.mensaje.balance);
        sessionStorage.setItem('user', JSON.stringify(acceso.mensaje));
        this.router.navigate(['perfil']);
      }
      else {
        this.alerta = acceso.mensaje;
        setTimeout(() => this.alerta = '', 2000);
      }
    }
  }

  checkFields() {
    if (this.correo === '') {
      this.alerta = 'Debe ingresar su correo electronico';
      setTimeout(() => this.alerta = '', 5000);
      return false;
    }
    else if (this.password === '') {
      this.alerta = 'Debe ingresar su contraseña';
      setTimeout(() => this.alerta = '', 5000);
      return false;
    }

    return true;
  }
}
