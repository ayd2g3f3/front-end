import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PrincipalComponent } from './principal/principal.component';
import { SignupComponent } from './signup/signup.component';
import { NotaComponent } from './nota/nota.component';


import { RecordatorioComponent } from './recordatorio/recordatorio.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'principal', component: PrincipalComponent },
  { path: 'nota', component: NotaComponent },
  { path: 'recordatorio', component: RecordatorioComponent },
  /*{ path: 'second-component', component: SecondComponent },
{ path: '',   redirectTo: '/first-component', pathMatch: 'full' }, // redirect to `first-component`
{ path: '**', component: FirstComponent },
{ path: '**', component: PageNotFoundComponent },  // Wildcard route for a 404 page*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}





